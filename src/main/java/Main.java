import model.Client;
import model.ClientService;
import model.Order;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ClientService clientService = new ClientService();
        List<Client> clientsList = new ArrayList<>();

        Client firstClient = new Client("Karlovych");
        Client secondClient = new Client("Test");

        Order order = new Order(1000);
        Order order2 = new Order(2000);
        Order order3 = new Order(3000);

        Order order4 = new Order(10020);
        Order order5 = new Order(20002);
        Order order6 = new Order(10000);

        firstClient.makeOrder(order, LocalDateTime.of(2019, Month.AUGUST, 5, 20,0));
        firstClient.makeOrder(order2, LocalDateTime.of(2019, Month.SEPTEMBER, 3, 20, 5));
        firstClient.makeOrder(order3, LocalDateTime.of(2019, Month.DECEMBER, 6, 8, 25));

        secondClient.makeOrder(order4, LocalDateTime.of(2019, Month.APRIL, 6, 20,0));
        secondClient.makeOrder(order5, LocalDateTime.of(2019, Month.SEPTEMBER, 5, 20,0));
        secondClient.makeOrder(order6, LocalDateTime.of(2019, Month.AUGUST, 2, 13,0));

        clientsList.add(firstClient);
        clientsList.add(secondClient);


        System.out.println(clientService.getLastOrder(clientsList));
        System.out.println(clientService.getOrdersByLastDay(clientsList));


        System.out.println(firstClient);
        System.out.println(secondClient);

    }
}
