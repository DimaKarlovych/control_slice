package strategy;

import model.Client;
import model.Order;

public class DiscounterImpl implements Discounter {
    @Override
    public double applyDiscount(Order order) {
        return order.getPrice()*0.8;
    }

    public boolean needDiscount(Client client) {
        int sum = 0;
        for(Order order : client.getOrdersList()) {
            sum += order.getPrice();
        }
        if(sum > 10000) {
            return true;
        }
        return false;
    }
}
