package strategy;

import model.Order;

public interface Discounter {
    public double applyDiscount(Order order);
}
