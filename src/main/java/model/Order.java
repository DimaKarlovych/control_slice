package model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString
public class Order implements Comparable<Order>{
    private int id;
    private double price;
    LocalDateTime localDateTime;

    public Order(int price) {
        this.id = ++id;
        this.price = price;
    }

    @Override
    public int compareTo(Order order) {
        if(this.getLocalDateTime().getYear() < order.getLocalDateTime().getYear()) {
            return 1;
        } else if (this.getLocalDateTime().getYear() > order.getLocalDateTime().getYear()) {
            return -1;
        } else if (this.getLocalDateTime().getYear() == order.getLocalDateTime().getYear()) {
            if(this.getLocalDateTime().getMonth().getValue() < order.getLocalDateTime().getMonth().getValue()) {
                return 1;
            } else if (this.getLocalDateTime().getMonth().getValue() > order.getLocalDateTime().getMonth().getValue()) {
                return -1;
            } else if (this.getLocalDateTime().getMonth().getValue() == order.getLocalDateTime().getMonth().getValue()) {
                if (this.getLocalDateTime().getDayOfMonth() < order.getLocalDateTime().getDayOfMonth()) {
                    return 1;
                } else if (this.getLocalDateTime().getDayOfMonth() > order.getLocalDateTime().getDayOfMonth()) {
                    return -1;
                }
            }
        }
        return 0;
    }
}
