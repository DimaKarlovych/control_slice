package model;

import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import strategy.DiscounterImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@ToString
@Getter
public class Client {
    private int id = 0;
    private String name;
    private List<Order> ordersList = new ArrayList<>();
    private List<Client> clientsList = new ArrayList<>();

    public Client(String name) {
        this.id = ++id;
        this.name = name;
    }

    public void makeOrder(@NonNull Order order, LocalDateTime date) {
        DiscounterImpl discounter = new DiscounterImpl();
        if(discounter.needDiscount(this)) {
            order.setPrice(discounter.applyDiscount(order));
        }
        order.setLocalDateTime(date);
        ordersList.add(order);
    }

    public List<Order> getOrdersList() {
        return ordersList;
    }
}
