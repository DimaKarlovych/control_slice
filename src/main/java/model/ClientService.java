package model;

import lombok.NonNull;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class ClientService {
    public List<Order> getLastOrder(@NonNull List<Client> clientsList) {
        return clientsList
                .stream()
                .flatMap(client -> client.getOrdersList().stream())
                .sorted().limit(1)
                .collect(Collectors.toList());
    }

//    public void getRichClient(List<Client> clientList) {
//        System.out.println(clientList
//                .stream()
//                .map(this::getSum).max(Comparator.comparingDouble(Double::intValue)));
//    }

    public List<Order> getOrdersByLastDay(@NonNull List<Client> clientList) {
        return clientList
                .stream()
                .flatMap(client -> client.getOrdersList().stream())
                .filter(this::sameDate).collect(Collectors.toList());
    }

    private boolean sameDate(Order order) {
        LocalDate currentDate = LocalDate.now();
        if(currentDate.getYear() == order.getLocalDateTime().getYear()
        && currentDate.getMonth().getValue() == order.getLocalDateTime().getMonth().getValue()
        && currentDate.getDayOfMonth() == order.getLocalDateTime().getDayOfMonth()) {
            return true;
        }
        return false;
    }

    private double getSum(Client client) {
        double sum = 0;
        for (Order order : client.getOrdersList()) {
            sum += order.getPrice();
        }
        return sum;
    }
}


