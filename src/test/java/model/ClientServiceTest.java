package model;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ClientServiceTest {
    List<Client> clients = new ArrayList<>();
    ClientService clientService = new ClientService();


    public void loadData() {
        Client firstClient = new Client("Karlovych");
        Client secondClient = new Client("Test");

        Order order = new Order(1000);
        Order order2 = new Order(2000);
        Order order3 = new Order(3000);

        Order order4 = new Order(10020);
        Order order5 = new Order(20002);
        Order order6 = new Order(10000);

        firstClient.makeOrder(order, LocalDateTime.of(2019, Month.AUGUST, 5, 20,0));
        firstClient.makeOrder(order2, LocalDateTime.of(2019, Month.SEPTEMBER, 3, 20, 5));
        firstClient.makeOrder(order3, LocalDateTime.of(2019, Month.DECEMBER, 6, 8, 25));

        secondClient.makeOrder(order4, LocalDateTime.of(2019, Month.APRIL, 6, 20,0));
        secondClient.makeOrder(order5, LocalDateTime.of(2019, Month.SEPTEMBER, 5, 20,0));
        secondClient.makeOrder(order6, LocalDateTime.of(2019, Month.AUGUST, 2, 13,0));

        clients.add(firstClient);
        clients.add(secondClient);
    }

    @Test
    void getLastOrderInputNullExpectedNPE() {
        loadData();
        clients = null;
        assertThrows(NullPointerException.class, () -> clientService.getLastOrder(clients));
    }

    @Test
    void getLastOrderInputNormalExpectedNPE() {
        loadData();
        assertEquals("[Order(id=1, price=3000.0, localDateTime=2019-12-06T08:25)]",
                clientService.getLastOrder(clients));
    }


    @Test
    void getOrdersByLastDayInputNullExpectedNPE() {
        loadData();
        clients = null;
        assertThrows(NullPointerException.class, () -> clientService.getOrdersByLastDay(clients));
    }
}